# Networking
The docker-compose files contain no specific networks. They can communicate with eachother based on host / container name as docker compose facilitates this.
Via portainer, a new network called 'hass-bridge' is created as a bridge network. 

After recreation of the nginx-proxy-manager container, the hass-network bridge is no longer linked to this container and in order to access hass.dare92.nl again, link the new container to this network.  
If this is not done, when trying to access hass.dare92.nl will result in a 502 bad gateway as the 192.168.122.x network created by qemu/kvm will not be reachable from that container.
