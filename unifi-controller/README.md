# Unifi controller is now the new network application (since 8.1)

# init-mongo.js
Ensure the `init-mongo.js` file exists with content below:
```js
db.getSiblingDB("unifi").createUser({user: "unifi", pwd: "GETYOUROWNPASSWORD", roles: [{role: "dbOwner", db: "unifi"}]});
db.getSiblingDB("unifi_stat").createUser({user: "unifi", pwd: "GETYOUROWNPASSWORD", roles: [{role: "dbOwner", db: "unifi_stat"}]});
```
(credentials are exemplary ofcourse)

Fill in the same credentials in the `.env` file (copy `.env.sample` to `.env` and edit it).
